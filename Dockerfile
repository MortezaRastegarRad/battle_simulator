# Dockerfile

# Use the official Python image from the Docker Hub
FROM python:3.8-slim

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Set the working directory
WORKDIR /app

# Install dependencies
COPY requirements.txt /app/
RUN pip install -r requirements.txt

# Copy the Django project files into the container
COPY . /app/

# Expose port 8888
EXPOSE 8888

# Run the application
CMD ["gunicorn", "--bind", "0.0.0.0:8888", "battle_simulator.wsgi:application"]