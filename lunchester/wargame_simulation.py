import numpy as np
from scipy.special import gamma


def a(j, k, alpha, dt):
    if j == 0:
        c = (k - 1) ** (alpha + 1) - (k - 1 - alpha) * k ** alpha
    elif j == k:
        c = 1
    else:
        c = (k - 1 - j + 2) ** (alpha + 1) + (k - 1 - j) ** (alpha + 1) - 2 * (k - j) ** (alpha + 1)
    return dt ** alpha / (alpha * (alpha + 1)) * c


def b(j, k, alpha, dt):
    return dt ** alpha / alpha * ((k - j) ** alpha - (k - 1 - j) ** alpha)


def f_dir(t, y, RA, RB, k1, k2):
    f1 = - RA(t) * y[1] - k1 * y[0]
    f2 = - RB(t) * y[0] - k2 * y[1]
    return np.array([f1, f2])


def f_indir(t, y, RA, RB, k1, k2):
    f1 = -RA(t) * y[1] * y[0] - k1 * y[0]
    f2 = -RB(t) * y[0] * y[1] - k2 * y[1]
    return np.array([f1, f2])


def f_asym(t, y, RA, RB, k1, k2):
    f1 = -RA(t) * y[1] - k1 * y[0]
    f2 = -RB(t) * y[0] * y[1] - k2 * y[1]
    return np.array([f1, f2])


def Adam_Bashforth_Moulton(y0, alpha, RA, RB, k1, k2, K, dt, model_inedx):
    if model_inedx in [1, 2]:
        f = f_dir
    elif model_inedx in [3, 4]:
        f = f_indir
    elif model_inedx in [5, 6]:
        f = f_asym

    t = 0
    y = np.zeros((2, K))
    y[:, 0] = y0
    tt = []
    for k in range(K - 1):
        s = 0
        cond = y[:, k] <= 0
        tt.append(k * dt)
        if np.sum(cond) == 0:
            for j in range(k + 1):
                s += b(j, k + 1, alpha, dt) * f(j * dt, y[:, j], RA, RB, k1, k2)
            y_p = y0 + 1.0 / gamma(alpha) * s

            s = 0
            for j in range(k + 1):
                s += a(j, k + 1, alpha, dt) * f(j * dt, y[:, j], RA, RB, k1, k2)
            s += dt ** alpha / (alpha * (alpha + 1)) * f((k + 1) * dt, y_p, RA, RB, k1, k2)

            y[:, k + 1] = y0 + 1.0 / gamma(alpha) * s
        else:
            break
    return y, tt


def Monte_Carlo_simulate_battle(orcs_init, elves_init, orcs_coeff, elves_coeff, end_time, step_size):
    t_vals = np.arange(0, end_time, step_size)
    x_vals = np.zeros_like(t_vals)
    y_vals = np.zeros_like(t_vals)

    x_vals[0] = orcs_init
    y_vals[0] = elves_init

    for i in range(1, len(t_vals)):
        dx = -elves_coeff * y_vals[i - 1]
        dy = -orcs_coeff * x_vals[i - 1]

        x_vals[i] = x_vals[i - 1] + dx * step_size
        y_vals[i] = y_vals[i - 1] + dy * step_size

        x_vals[i] = max(x_vals[i], 0)
        y_vals[i] = max(y_vals[i], 0)

    return t_vals, x_vals, y_vals
