import base64
from bidi import algorithm as bidialg
from django.shortcuts import render
from .wargame_simulation import Adam_Bashforth_Moulton, Monte_Carlo_simulate_battle
import numpy as np
import matplotlib.pyplot as plt
import io
import arabic_reshaper


def make_farsi_text(x):
    reshaped_text = arabic_reshaper.reshape(x)
    farsi_text = bidialg.get_display(reshaped_text)
    return farsi_text


def index(request):
    if request.method == 'POST':
        # Get the input values from the form
        model_index = int(request.POST.get('model_index'))
        alpha = float(request.POST.get('alpha'))
        r_a = float(request.POST.get('r_a'))
        r_b = float(request.POST.get('r_b'))
        a_0 = float(request.POST.get('a_0'))
        b_0 = float(request.POST.get('b_0'))

        if model_index in [2, 4, 5]:
            k1 = float(request.POST.get('k1'))
            k2 = float(request.POST.get('k2'))
        else:
            k1 = 0
            k2 = 0

        if model_index == 7:
            t_vals, x_vals, y_vals = Monte_Carlo_simulate_battle(a_0, b_0, r_b, r_a, 10000, 0.1)
            y_app = np.array([x_vals, y_vals])
            x_vals = [i for i in x_vals if i != float(0)]
            y_vals = [i for i in y_vals if i != float(0)]
            tt_len = len(x_vals) if len(x_vals) < len(y_vals) else len(y_vals)
            tt = [i for i in range(tt_len + 2)]
        else:
            step = 2001
            dt = 0.005
            y0 = np.array([a_0, b_0])
            RA = lambda t: r_a
            RB = lambda t: r_b

            y_app, tt = Adam_Bashforth_Moulton(y0, alpha, RA, RB, k1, k2, step, dt, model_index)

        fig, ax = plt.subplots(figsize=(8, 6))
        ax.plot(tt, y_app[0, :len(tt)], label=make_farsi_text('گروه ۱'), color='blue')
        ax.plot(tt, y_app[1, :len(tt)], label=make_farsi_text('گروه ۲'), color='red')
        ax.set_xlabel(make_farsi_text('زمان'))
        ax.set_ylabel(make_farsi_text('اندازه جمعیت'))
        ax.set_title(make_farsi_text('شبیه‌سازی معادلات لانچستر'))

        ax.legend()
        buf = io.BytesIO()
        fig.savefig(buf, format='png')
        image_base64 = base64.b64encode(buf.getvalue()).decode('utf-8')

        # Render the template with the image
        context = {
            'image_base64': image_base64,
            'model_index': model_index,
            'alpha': alpha,
            'r_a': r_a,
            'r_b': r_b,
            'a_0': a_0,
            'b_0': b_0,
            'k1': k1,
            'k2': k2,
            'a_app': [round(i, 2) for i in y_app[0, :len(tt)]],
            'b_app': [round(i, 2) for i in y_app[1, :len(tt)]]
        }
        return render(request, 'index.html', context)

    else:
        context = {
            'model_index': 1,
            'alpha': 1.0,
            'r_a': 1,
            'r_b': 1,
            'a_0': 1,
            'b_0': 2,
            'k1': 0.0,
            'k2': 0.0,
            'a_app': [1],
            'b_app': [2]
        }
        return render(request, 'index.html', context)
